#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h> 
#include <time.h> 
#include<sys/time.h>

int main(int argc, char *argv[])
{
    int sockfd = 0, n = 0, connfd = 0;
    char t1[1024];
	char t2[1024];
	char t3[1024];
	char t4[1024];
	char tnew[1024];
    time_t ticks,tick; //struct taken from time.h 
    struct sockaddr_in serv_addr; 
   int ret;
   struct tm tm;
   time_t t;

    if(argc != 2)
    {
        printf("\n Usage: %s <ip of server> \n",argv[0]);
        return 1;
    } 

    memset(t1, '0',sizeof(t1));
	memset(t2, '0',sizeof(t2));
	memset(t3, '0',sizeof(t3));
	memset(t4, '0',sizeof(t4));
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Error : Could not create socket \n");
        return 1;
    } 

    memset(&serv_addr, '0', sizeof(serv_addr)); 
	//take the server defination 
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(5000); 

    if(inet_pton(AF_INET, argv[1], &serv_addr.sin_addr)<=0)
    {
        printf("\n inet_pton error occured\n");
        return 1;
    } 

	//try to connect to server
    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
       printf("\n Error : Connect Failed \n");
       return 1;
    } 
	
	//read from server
    while ( (n = read(sockfd, t1, sizeof(t1)-1)) > 0)
    {
        t1[n] = 0;

        if(fputs(t1, stdout) == EOF)
        {
            printf("\n Error : Fputs error\n");
        }
	ticks = time(NULL);
	/*strftime(t2,sizeof(t2), "%T", &ticks); */	
	snprintf(t2, sizeof(t2), "%.24s\r\n", ctime(&ticks));
	fputs(t2,stdout);
	strptime(t1, "%a %b %d %H:%M:%S %Y", &tm);
	/*printf("year: %d; month: %d; day: %d;\n",
        tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday);
	printf("hour: %d; minute: %d; second: %d\n",
        tm.tm_hour, tm.tm_min, tm.tm_sec);
	printf("week day: %d; year day: %d\n", tm.tm_wday, tm.tm_yday);*/


	tm.tm_isdst = -1;      /* Not set by strptime(); tells mktime()
                          to determine whether daylight saving time
                          is in effect */
	/*t = mktime(&tm);*/
	/* secont part*/
	tick = time(NULL);
	snprintf(t3, sizeof(t3), "%.24s\r\n", ctime(&tick));
	write(sockfd, t2, strlen(t2)); 
	write(sockfd, t3, strlen(t3)); 
	fputs(t3,stdout);
		
	 while ( (n = read(sockfd, tnew, sizeof(tnew)-1)) > 0)
    	{
        tnew[n] = 0;
	struct tm snew;
	strptime(tnew, "%a %b %d %H:%M:%S %Y", &snew);
	snew.tm_isdst = -1;
	t = mktime(&snew);
	struct tm* tm_ptr = localtime(&t);
	if (tm_ptr)
  	{
    	const struct timeval tv = {mktime(tm_ptr), 0};
    	settimeofday(&tv, 0);
  	}
	} 
   }
    if(n < 0)
    {
        printf("\n Read error \n");
	
    } 

    return 0;
}
