#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h> 
#include<sys/time.h>

int main(int argc, char *argv[])
{
     int sockfd = 0, n = 0;
    int listenfd = 0, connfd = 0;
    struct sockaddr_in serv_addr; 

    char t1[1024];
	char t2[1024];
	char t3[1024];
	char t4[1024];
	char tnew[1024];
	char tset[1024];
    time_t ticks,tick,tick2; 
    struct tm * timeinfo; //struct taken from time.h

	//creating socket
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&serv_addr, '0', sizeof(serv_addr));
    memset(t1, '0',sizeof(t1));
	memset(t2, '0',sizeof(t2));
	memset(t3, '0',sizeof(t3));
	memset(t4, '0',sizeof(t4));
	
	//server initialization and defination
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(5000); 
	
	//server binding
    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); 
	
	//server ready to listen
    listen(listenfd, 10); 

    while(1)
    {
        //server attach to client
		connfd = accept(listenfd, (struct sockaddr*)NULL, NULL); 

        ticks = time(NULL);
	/*strftime(t1, sizeof(t1),"%T", &ticks);  */      
	snprintf(t1, sizeof(t1), "%.24s\r\n", ctime(&ticks));

	fputs(t1,stdout);
        write(connfd, t1, strlen(t1)); 
	 while ( (n = read(connfd, t2, sizeof(t2)-1)) > 0)
    	{
        t2[n] = 0;
	
	 while ( (n = read(connfd, t3, sizeof(t3)-1)) > 0)
    	{
        t3[n] = 0;
	
	tick = time(NULL);
	snprintf(t4, sizeof(t4), "%.24s\r\n", ctime(&tick));
	fputs(t4,stdout);
	struct tm s1;
	struct tm s2;
	struct tm s3;
	struct tm s4;
	struct tm soff;
	strptime(t1, "%a %b %d %H:%M:%S %Y", &s1);
	s1.tm_isdst = -1;
	strptime(t2, "%a %b %d %H:%M:%S %Y", &s2);
	s2.tm_isdst = -1;
	strptime(t3, "%a %b %d %H:%M:%S %Y", &s3);
	s3.tm_isdst = -1;
	strptime(t4, "%a %b %d %H:%M:%S %Y", &s4);
	s4.tm_isdst = -1;
	
	soff.tm_year=(s2.tm_year-s4.tm_year-s1.tm_year+s3.tm_year)/2;
	soff.tm_mon=(s2.tm_mon-s4.tm_mon-s1.tm_mon+s3.tm_mon)/2;
	soff.tm_mday=(s2.tm_mday-s4.tm_mday-s1.tm_mday+s3.tm_mday)/2;
	soff.tm_hour=(s2.tm_hour-s4.tm_hour-s1.tm_hour+s3.tm_hour)/2;
	soff.tm_min=(s2.tm_min-s4.tm_min-s1.tm_min+s3.tm_min)/2;
	soff.tm_sec=(s2.tm_sec-s4.tm_sec-s1.tm_sec+s3.tm_sec)/2;
	
	soff.tm_isdst = -1;	

	printf("year: %d; month: %d; day: %d;\n",
        soff.tm_year, soff.tm_mon, soff.tm_mday);
	printf("hour: %d; minute: %d; second: %d\n",
        soff.tm_hour, soff.tm_min, soff.tm_sec);
	/*printf("week day: %d; year day: %d\n", soff.tm_wday, soff.tm_yday);*/

	struct tm snew;
	tick2 = time(NULL);
	snprintf(tnew, sizeof(tnew), "%.24s\r\n", ctime(&tick2));
	strptime(tnew, "%a %b %d %H:%M:%S %Y", &snew);
	snew.tm_isdst = -1;
	
	snew.tm_year= snew.tm_year+soff.tm_year;
	snew.tm_mon= snew.tm_mon+soff.tm_mon;
	snew.tm_mday= snew.tm_mday+soff.tm_mday;
	snew.tm_hour= snew.tm_hour+soff.tm_hour;
	snew.tm_min= snew.tm_min+soff.tm_min;
	snew.tm_sec= snew.tm_sec+soff.tm_sec;

	snprintf(tset, sizeof(tset), "%.24s\r\n", asctime(&snew));
	write(connfd, tset, strlen(tset));	
	fputs(tset,stdout);
		
		//server dettach to client
        close(connfd);
        sleep(1);
     	}
     	}
     }
}
