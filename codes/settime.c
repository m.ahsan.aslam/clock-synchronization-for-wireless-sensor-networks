#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h> 
#include <time.h> 
#include<sys/time.h>

int main () {
   int ret;
   struct tm tm;
   time_t t;
   char buffer[80]="Tue Nov 27 02:58:12 2018";

  	strptime(buffer, "%a %b %d %H:%M:%S %Y", &tm);
	printf("year: %d; month: %d; day: %d;\n",
        tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday);
	printf("hour: %d; minute: %d; second: %d\n",
        tm.tm_hour, tm.tm_min, tm.tm_sec);
	printf("week day: %d; year day: %d\n", tm.tm_wday, tm.tm_yday);


	tm.tm_isdst = -1;      /* Not set by strptime(); tells mktime()
                          to determine whether daylight saving time
                          is in effect */
	t = mktime(&tm);
	struct tm* tm_ptr = localtime(&t);
	if (tm_ptr)
  	{
    	const struct timeval tv = {mktime(tm_ptr), 0};
    	settimeofday(&tv, 0); //set the time of the system from struct
  	}




   return(0);
}
